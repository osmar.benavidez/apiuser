package com.nisum.userapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nisum.userapi.api.dao.repository.UserRepository;
import com.nisum.userapi.api.dto.PhoneDTO;
import com.nisum.userapi.api.dto.UserDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class UserapiApplicationTests {

    private static final ObjectMapper om = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    UserRepository userRepository;

    String AuthenticateRQ="{\n" +
            "    \"username\":\"admin\",\n" +
            "    \"password\":\"123\",\n" +
            "    \"appname\":\"POSTMAN\"\n" +
            "}";

    String TOKEN=null;
    @BeforeEach
    void setup(TestInfo testInfo) throws Exception {
        if(testInfo!=null && testInfo.getTags().contains("AUTHENTICATE"))
            return;
        MvcResult mvcResult= GetCallToken(AuthenticateRQ).andReturn();
        TOKEN=String.valueOf(new Gson().fromJson(mvcResult.getResponse().getContentAsString(), Map.class).get("token"));
    }
    @Test
    void Save_PassingValidValue_Successfully() throws Exception {

        assertNotNull(TOKEN);

        PhoneDTO phone=new PhoneDTO();
        phone.setCityCode("2022");
        phone.setCountryCode("2021");
        phone.setNumber("1111111");

        UserDTO user=new UserDTO();
        user.setEmail("carlos.lopez@gmail.com");
        user.setFullname("Carlos Lopez");
        user.setName("clopez");
        user.setPassword("Managua*123");
        user.setPhones(new HashSet<PhoneDTO>(Arrays.asList(phone)));

        RequestBuilder request = MockMvcRequestBuilders
                .post("/user/save")
                .content(om.writeValueAsString(user))
                .header("Authorization", TOKEN)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON);

        MvcResult mvcResult=mockMvc.perform(request).andExpect(status().isCreated()).andReturn();
        assertThat(String.valueOf(new Gson().fromJson(mvcResult.getResponse().getContentAsString(), Map.class).get("id"))).isNotNull();

    }

    @Tag("AUTHENTICATE")
    @Test
    void Authenticate_PasingUserNameAndPassword_Successfully() throws Exception {
        MvcResult mvcResult= GetCallToken(AuthenticateRQ).andReturn();
        assertNotNull(String.valueOf(new Gson().fromJson(mvcResult.getResponse().getContentAsString(), Map.class).get("token")));
    }

    @Tag("AUTHENTICATE")
    @Test
    void Authenticate_PasingWrongUserNameAndPassword_Successfully() throws Exception {
        AuthenticateRQ="{\n" +
                "    \"username\":\"adminnn\",\n" +
                "    \"password\":\"123\",\n" +
                "    \"appname\":\"POSTMAN\"\n" +
                "}";
        ResultActions resultActions= GetCallToken(AuthenticateRQ);
        MvcResult mvcResult=resultActions.andExpect(status().isUnauthorized()).andReturn();
        String error =  mvcResult.getResolvedException().getMessage();
        assertTrue(error.equals("USER_DISABLED") || error.equals("INVALID_CREDENTIALS"));
    }

    ResultActions GetCallToken(String... content) throws Exception
    {
        RequestBuilder request = MockMvcRequestBuilders
                .post("/authenticate")
                .content((content==null || content.length==0)?AuthenticateRQ:content[0])
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON);
        return mockMvc.perform(request);
    }

}
