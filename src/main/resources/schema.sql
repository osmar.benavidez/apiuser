
DROP TABLE IF EXISTS session;
DROP TABLE IF EXISTS phone;
DROP TABLE IF EXISTS user_role;
DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    id UUID PRIMARY KEY,
    fullname VARCHAR(200) NOT NULL,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(100) NOT NULL,
    created TIMESTAMP NOT NULL,
    modified TIMESTAMP,
    is_active BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE role (
                      id UUID PRIMARY KEY,
                      name VARCHAR(50) NOT NULL,
                      created TIMESTAMP NOT NULL,
                      modified TIMESTAMP,
                      is_active BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE user_role (
    user_id  UUID NOT NULL,
    role_id  UUID NOT NULL,
    created TIMESTAMP NOT NULL,
    modified TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (role_id) REFERENCES role(id)
);



CREATE TABLE phone(
   id INT AUTO_INCREMENT PRIMARY KEY,
   user_id UUID NOT NULL,
   number VARCHAR(25) NOT NULL,
   citycode VARCHAR(4) NOT NULL,
   countrycode varchar(4) NOT NULL,
   created TIMESTAMP NOT NULL,
   modified TIMESTAMP,
   FOREIGN KEY (user_id) REFERENCES user(id)
);
CREATE TABLE session
(
    id UUID PRIMARY KEY,
    appname VARCHAR(100) NOT NULL,
    user_id  UUID NOT NULL,
    token   VARCHAR(350),
    expirated TIMESTAMP NOT NULL,
    modified TIMESTAMP,
    created TIMESTAMP NOT NULL,
    is_active BOOLEAN NOT NULL DEFAULT TRUE,
    FOREIGN KEY (user_id) REFERENCES user(id)
);