-- Insertando usuario
INSERT INTO user (id, fullname,name, password, email,created, modified,is_active) VALUES
( '46475c56-ddbe-4171-9921-08a7ecdd4a39',
  'Administrador',
  'admin',
  '$2a$10$82DjAl3la8ETkDy/aTYL5.BukRmggIgLwKcQ2u2thQ1ThPKnsmjha',
  'osmar.benavidez@gmail.com',
  '2022-08-11 04:05',
  NULL,
  true
  );

INSERT INTO user (id, fullname,name, password, email,created, modified,is_active) VALUES
    ( '43375c56-ddbe-4171-9921-08a7ecdd4a67',
      'Osmar Benavidez',
      'obenavidez',
      '$2a$10$82DjAl3la8ETkDy/aTYL5.BukRmggIgLwKcQ2u2thQ1ThPKnsmjha',
      'osmar.benavidez@gmail.com',
      '2022-08-11 04:05',
      NULL,
      true
    );


-- PHONES
INSERT INTO phone(user_id,number,citycode,countrycode,created,modified)
values('46475c56-ddbe-4171-9921-08a7ecdd4a39',81387171,'22','505',CURRENT_TIMESTAMP(),null);

INSERT INTO role (id, name ,created,modified,is_active) VALUES
('3e92959e-19a7-11ed-861d-0242ac120002', 'ADMIN','2022-08-11 04:05',NULL,true);

INSERT INTO role (id, name ,created,modified,is_active) VALUES
    ('3e92959e-19a7-11ed-861d-0242ac120003', 'USERWS','2022-08-11 04:05',NULL,true);

-- PHONES
INSERT INTO user_role (user_id, role_id ,created,modified) VALUES
    ( '46475c56-ddbe-4171-9921-08a7ecdd4a39','3e92959e-19a7-11ed-861d-0242ac120002','2022-08-11 04:05',NULL);

-- PHONES
INSERT INTO user_role (user_id, role_id ,created,modified) VALUES
    ( '43375c56-ddbe-4171-9921-08a7ecdd4a67','3e92959e-19a7-11ed-861d-0242ac120003','2022-08-11 04:05',NULL);
