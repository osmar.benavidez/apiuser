package com.nisum.userapi.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nisum.userapi.api.dao.entidad.PhoneEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PhoneDTO implements Serializable {

    public PhoneDTO() {
    }

    /** Phone Number*/
    @ApiModelProperty(value = "Phone Number.",
            name = "number",
            dataType = "String",
            required = true,
            position = 2
    )
    @NotEmpty(message = "Phone Number is required.")
    @JsonProperty("number")
    private String number;

    /** City code*/
    @ApiModelProperty(value = "City Code Number.",
            name = "cityCode",
            dataType = "String",
            required = true,
            position = 3
    )
    @NotEmpty(message = "City Code Number is required")
    @JsonProperty("citycode")
    private String cityCode;

    /** Country Code*/
    @ApiModelProperty(value = "Country Code Number ",
            name = "countryCode",
            dataType = "String",
            required = true,
            position = 4
    )
    @NotEmpty(message = "Country Code Number is required.")
    @JsonProperty("contrycode")
    private String countryCode;

    public PhoneDTO(String number, String cityCode, String countryCode) {
        this.number = number;
        this.cityCode = cityCode;
        this.countryCode = countryCode;
    }

    public PhoneDTO(PhoneEntity phoneEntity) {
        this(phoneEntity.getNumber(), phoneEntity.getCityCode(),phoneEntity.getCountryCode());
    }

}
