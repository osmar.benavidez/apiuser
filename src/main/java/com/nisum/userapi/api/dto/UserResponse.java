package com.nisum.userapi.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponse {

    /***user ID*/
    @ApiModelProperty(value = "User ID.",
            name = "id",
            dataType = "Integer",
            required = false,
            position = 1
    )
    @JsonProperty("id")
    private UUID id;

    /**User date created  */
    @JsonProperty("created")
    private LocalDateTime created;

    /**User date updated  */
    @JsonProperty("modified")
    private LocalDateTime modified;

    /**Last login  */
    @JsonProperty("last_login")
    private LocalDateTime lastLogin;

    /**user token  */
    @ApiModelProperty(value = "Token.",
            name = "token",
            dataType = "String",
            required = false,
            position = 5
    )
    @JsonProperty("token")
    private String token;

    @JsonProperty("active")
    private Boolean active;

    public UserResponse(){

    }
}
