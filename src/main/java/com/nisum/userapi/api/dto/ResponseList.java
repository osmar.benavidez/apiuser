package com.nisum.userapi.api.dto;

import lombok.Data;

import java.util.List;

@Data
public class ResponseList<T> {
    public ResponseList(List<T> objlist) {
        this.objlist = objlist;
    }
    List<T> objlist;
}
