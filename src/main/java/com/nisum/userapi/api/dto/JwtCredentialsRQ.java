package com.nisum.userapi.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JwtCredentialsRQ  implements Serializable {

        /**User Request*/
        @ApiModelProperty(value = "User name",
                name = "username",
                dataType = "String",
                required = true,
                position = 1
        )
        @NotNull(message = "Username is required")
        private String username;

        /**Password User Request*/
        @ApiModelProperty(value = "Password.",
                name = "password",
                dataType = "String",
                required = true,
                position = 2
        )
        @NotNull(message = "Password is required")
        private String password;

        /**Password User Request*/
        @ApiModelProperty(value = "AppName.",
                name = "appname",
                dataType = "String",
                required = true,
                position = 3
        )
        @NotEmpty(message = "Application Name is required")
        @NotNull(message = "Application Name is required")
        private String appname;
}
