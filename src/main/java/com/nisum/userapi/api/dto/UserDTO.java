package com.nisum.userapi.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nisum.userapi.api.dao.entidad.UserEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

    /** Full Name */
    @ApiModelProperty(value = "Full User Name.",
            name = "id",
            dataType = "name",
            required = true,
            position = 2
    )
    @NotEmpty(message = "Full Name is required.")
    @JsonProperty("fullname")
    private String fullname;

    /** User Name */
    @ApiModelProperty(value = "User Name.",
            name = "id",
            dataType = "name",
            required = true,
            position = 2
    )
    @NotEmpty(message = "User Name is required.")
    @JsonProperty("name")
    private String name;

    /***User email
     */
    @ApiModelProperty(value = "Email.",
            name = "email",
            dataType = "String",
            required = true,
            position = 3
    )
    @NotNull(message = "Email is required")
    @Email(regexp = "[A-Za-z0-9._%-+]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}", message = "invalid email")
    @JsonProperty("email")
    private String email;

    /**
     * password user.
     */
    @ApiModelProperty(value = "Password.",
            name = "pasword",
            dataType = "String",
            required = true,
            position = 4
    )
    @NotEmpty(message = "Password is required")
    @Column(name = "password")
    @JsonProperty("password")
    private String password;

    /** User phone list
     */
    @ApiModelProperty(value = "Phones",
            name = "phones",
            dataType = "List",
            required = true,
            position = 7
    )
    @NotNull(message = "Phone User is required")
    @Size(min = 1, message = "At least one phone is required")
    @JsonProperty("phones")
    private @Valid
    Set<PhoneDTO> phones;


    public UserDTO(){

    }

    public UserDTO(String fullname, String name, String email, String password, Set<PhoneDTO> phones) {
        this.fullname = fullname;
        this.name = name;
        this.email = email;
        this.password = password;
        this.phones = phones;
    }

    public UserDTO(UserEntity userEntity) {
        this( userEntity.getFullname(),userEntity.getName(),userEntity.getEmail(),  userEntity.getPassword(), userEntity.getPhones().stream().map(p -> new PhoneDTO(p)).collect(Collectors.toSet()));
    }
}
