package com.nisum.userapi.api.dao.entidad;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@Builder
@AllArgsConstructor
@Data
@Entity
@Table(name = "session")
public class SessionEntity extends Auditable {
    @Id
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    @GeneratedValue(generator = "uuid")
    @Column(name = "id", nullable = false)
    private UUID id;
    @Basic
    @Column(name = "appname", nullable = false)
    private String appName;
    @Basic
    @Column(name = "user_id", nullable = false)
    private UUID userId;
    @Basic
    @Column(name = "token", nullable = false )
    private String token;
    @Basic
    @Column(name = "expirated", nullable = false)
    private Timestamp expirated;
    /** user status. */
    @Basic
    @Column(name = "is_active" , columnDefinition = "boolean default true")
    private Boolean isactive;

    public SessionEntity() {
    }
}
