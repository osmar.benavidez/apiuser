package com.nisum.userapi.api.dao.repository;

import com.nisum.userapi.api.dao.entidad.SessionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SessionRepository extends JpaRepository<SessionEntity, UUID> {
    @Query(value="select * from session s where s.appname=:appname and s.user_id=:userid and s.expirated>CURRENT_TIMESTAMP() order by s.expirated desc limit 1",nativeQuery=true)
    SessionEntity getCurrent(@Param("appname") String appname,@Param("userid") UUID  userid);


    @Modifying
    @Query(value = "UPDATE session  set is_active =false where user_id=:userid and token=:token",
            nativeQuery = true)
    int  flushSession(@Param("userid") UUID  userid,@Param("token") String  token);

    @Modifying
    @Query(value = "UPDATE session  set is_active =false where user_id=:userid",
            nativeQuery = true)
    int  flushAllUserSession(@Param("userid") UUID  userid);
}
