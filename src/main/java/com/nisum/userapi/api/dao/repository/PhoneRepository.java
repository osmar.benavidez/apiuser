package com.nisum.userapi.api.dao.repository;

import com.nisum.userapi.api.dao.entidad.PhoneEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<PhoneEntity, Integer> {
}
