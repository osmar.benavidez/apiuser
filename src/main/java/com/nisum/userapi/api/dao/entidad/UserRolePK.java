package com.nisum.userapi.api.dao.entidad;

import lombok.Data;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Data
@Embeddable
public class UserRolePK implements Serializable {
    private static final long serialVersionUID = -1469915357139100566L;
    protected UUID user_id;
    protected UUID role_id;

}
