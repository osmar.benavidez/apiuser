package com.nisum.userapi.api.dao.entidad;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.UUID;


/**
 * @author Osmar Benavidez, 09/08/2022
 * @version 1.0
 * @see Getter
 * @see Setter
 * @see EqualsAndHashCode
 * @see ToString
 * @see Entity
 * @see Table
 * @see Serializable
 */
@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "phone")
public class PhoneEntity extends Auditable implements Serializable{

    /** user identifier.  */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /** phone number. */
    @NotEmpty(message = "Number is required.")
    @Column(name = "number")
    private String number;

    /** city code*/
    @Column(name = "citycode")
    private String cityCode;

    /** country code*/
    @Column(name = "countrycode")
    private String countryCode;

//    @Column(nullable=false)
//    private UUID user_id;
    /**owner phone user*/
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private UserEntity user;

    public PhoneEntity() {

    }

    public PhoneEntity(Integer id, String number, String cityCode, String countryCode, UserEntity user) {
        this.id = id;
        this.number = number;
        this.cityCode = cityCode;
        this.countryCode = countryCode;
        this.user = user;
    }
}
