package com.nisum.userapi.api.dao.entidad;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Builder
@AllArgsConstructor
@Data
@Entity
@Table(name = "user_role")
public class UserRoleEntity implements Serializable {

    @EmbeddedId
    private UserRolePK userRolePK;

    public UserRoleEntity() {

    }
}
