package com.nisum.userapi.api.dao.entidad;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "user")
public class UserEntity extends Auditable implements Serializable {
    @Id
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    @GeneratedValue(generator = "uuid")
    @Column(name = "id", nullable = false)
    private UUID id;

    /** full user name. */
    @Basic
    @Column(name = "fullname")
    @NotEmpty(message = "Full User Name is required.")
    private String fullname;

    /** user name. */
    @Basic
    @Column(name = "name", nullable = false)
    @NotEmpty(message = "User Name is required.")
    private String name;

    /** user password */
    @NotEmpty(message = "Password is required.")
    @Column(name = "password")
    private String password;

    /** user email. */
    @Email(regexp = "[A-Za-z0-9._%-+]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}", message = "invalid email")
    @NotNull(message = "Email is required")
    @Column(name = "email")
    private String email;

    /** user status. */
    @Basic
    @Column(name = "is_active" , columnDefinition = "boolean default true")
    private Boolean isactive;

    /** user phones.*/

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<PhoneEntity> phones = new HashSet<>();

    public UserEntity() {
    }

    public UserEntity(UUID id, String fullname, String name, String password, String email, Boolean isactive, Set<PhoneEntity> phones) {
        this.id = id;
        this.fullname = fullname;
        this.name = name;
        this.password = password;
        this.email = email;
        this.isactive = isactive;
        this.phones = phones;
    }
}
