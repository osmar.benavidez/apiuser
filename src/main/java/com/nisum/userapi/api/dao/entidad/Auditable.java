package com.nisum.userapi.api.dao.entidad;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable {

    @CreatedDate
    @Column(name = "created", updatable = false)
    //@Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime creationDate;

    @LastModifiedDate
    @Column(name = "modified")
   // @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime modifiedDate;
}
