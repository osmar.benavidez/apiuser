package com.nisum.userapi.api.dao.repository;

import com.nisum.userapi.api.dao.entidad.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    UserEntity findByName(String value);

    UserEntity findByEmail(String email);
}
