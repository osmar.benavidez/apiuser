package com.nisum.userapi.api.dao.repository;

import com.nisum.userapi.api.dao.entidad.UserRoleEntity;
import com.nisum.userapi.api.dao.entidad.UserRolePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.UUID;

public interface UserRoleRepository extends JpaRepository<UserRoleEntity, UserRolePK> {

    @Query(value = "select user_id User_Id,role_id Role_Id,r.name as role_name from user_role ur join role r on ur.role_id=r.id where user_id=:userId",nativeQuery = true)
    ArrayList<UserRoleDTO> getUserRolesByUserId(@Param("userId") UUID userId);

    interface UserRoleDTO{
        UUID getUser_Id();
        UUID getRole_Id();
        String getRole_Name();
    }

}
