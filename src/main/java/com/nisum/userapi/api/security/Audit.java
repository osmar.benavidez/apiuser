package com.nisum.userapi.api.security;


import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Audit<T> {
    @CreatedBy
    @Column(name="createdby")
    protected T createdBy;
    @LastModifiedBy
    @Column(name="updatedby")
    protected T updatedby;
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created")
    protected Date created;
    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated")
    protected Date updated;
}