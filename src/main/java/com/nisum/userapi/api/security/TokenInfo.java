package com.nisum.userapi.api.security;

import lombok.Data;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
@Data
public class TokenInfo{
    String tocken;
    Timestamp expirationDate;
    public TokenInfo(String tocken, Timestamp expirationDate) {
        this.tocken = tocken;
        this.expirationDate = expirationDate;
    }
}