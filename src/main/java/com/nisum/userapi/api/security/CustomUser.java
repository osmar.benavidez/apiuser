package com.nisum.userapi.api.security;

import com.nisum.userapi.api.bl.service.CustomUserDetails;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.UUID;

public class CustomUser extends User implements CustomUserDetails {
    private UUID userid;
    private String fullname;
    private String email;

    public CustomUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public CustomUser(UUID userid, String username, String email,String fullname,  String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.userid=userid;
        this.email=email;
        this.fullname=fullname;
    }

    @Override
    public UUID getUserID() {
        return this.userid;
    }

    @Override
    public String getFullName() {
        return fullname;
    }

    @Override
    public String getEmail() {
        return email;
    }

}
