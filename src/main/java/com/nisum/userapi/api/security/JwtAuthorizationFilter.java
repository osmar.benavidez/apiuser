package com.nisum.userapi.api.security;

import com.nisum.userapi.api.bl.service.CustomUserDetails;
import com.nisum.userapi.api.bl.service.JwtAuthService;
import com.nisum.userapi.api.utils.Constants;
import com.nisum.userapi.api.utils.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthorizationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtAuthService userService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException
    {

        String authorizationHeader = request.getHeader("Authorization");

        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            final String token = authorizationHeader.replace(Constants.TOKEN_BEARER_PREFIX + " ", "");
            String username  = TokenProvider.getUserName(token);
            if(username != null && SecurityContextHolder.getContext().getAuthentication() == null){
                CustomUserDetails userDetails = (CustomUserDetails) userService.loadUserByUsername(username);
                if(TokenProvider.validateToken(token, userDetails)){
                    UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authToken);
                }else
                    userService.flushSession(userDetails.getUserID(),token);
            }
        }
        filterChain.doFilter(request, httpServletResponse);

    }
}
