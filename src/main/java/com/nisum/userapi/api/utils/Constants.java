package com.nisum.userapi.api.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class Constants
{
    public static String APPNAME;
    public static String REQUEST_TOKEN_URL;
    public static String AUTHORITIES_KEY ;
    public static String SIGNING_KEY;
    public static int ACCESS_TOKEN_VALIDITY_SECONDS;
    public static String ISSUER_TOKEN;
    public static String HEADER_AUTHORIZATION_KEY;
    public static String TOKEN_BEARER_PREFIX;
    public static String EMAIL_REGEX;
    public static String PASSWORD_REGEX;

    @Value("${APPNAME}")
    public void setAppName(String appname) {
        APPNAME = appname;
    }

    @Value("${REQUEST_TOKEN_URL}")
    public void setCreateSessionPath(String url) {
        REQUEST_TOKEN_URL = url;
    }

    @Value("${AUTHORITIES_KEY}")
    public void setAuthorities(String authorities) {
        AUTHORITIES_KEY = authorities;
    }

    @Value("${SIGNING_KEY}")
    public void setSigningKey(String signingKey) {
        SIGNING_KEY = signingKey;
    }

    @Value("${ACCESS_TOKEN_VALIDITY_SECONDS}")
    public void setValiditySecond(String second) {
        ACCESS_TOKEN_VALIDITY_SECONDS = Integer.valueOf(second);
    }

    @Value("${ISSUER_TOKEN}")
    public void setIssuerToken(String issuer) {
        ISSUER_TOKEN = issuer;
    }

    @Value("${HEADER_AUTHORIZATION_KEY}")
    public void setHeaderAuthorization(String headerAuthorization) {
        HEADER_AUTHORIZATION_KEY = headerAuthorization;
    }

    @Value("${TOKEN_BEARER_PREFIX}")
    public void setPrefixToken(String prefixToken) {
        TOKEN_BEARER_PREFIX = prefixToken;
    }

    @Value("${EMAIL_REGEX}")
    public void setEmailRegex(String regex) {
        EMAIL_REGEX = regex;
    }

    @Value("${PASSWORD_REGEX}")
    public void setPasswordRegex(String regex) {
        PASSWORD_REGEX = regex;
    }


    public static Throwable findCauseUsingPlainJava(Throwable throwable) {
        Objects.requireNonNull(throwable);
        Throwable rootCause = throwable;
        while (rootCause.getCause() != null && rootCause.getCause() != rootCause)
            rootCause = rootCause.getCause();
        return rootCause;
    }
}