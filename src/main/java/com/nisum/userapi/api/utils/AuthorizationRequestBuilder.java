package com.nisum.userapi.api.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthorizationRequestBuilder {
    private String userName;

    private String password;

    @JsonProperty("user")
    public AuthorizationRequestBuilder userName(String userName) {
        this.userName = userName;
        return this;
    }

    public AuthorizationRequestBuilder password(String password) {
        this.password = password;
        return this;
    }

    public AuthorizationRequest build() {
        return new AuthorizationRequest(this.userName, this.password);
    }

    public String toString() {
        return "AuthorizationRequest.AuthorizationRequestBuilder(userName=" + this.userName + ", password=" + this.password + ")";
    }
}

