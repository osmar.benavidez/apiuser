package com.nisum.userapi.api.utils;

import com.nisum.userapi.api.bl.service.CustomUserDetails;
import com.nisum.userapi.api.dao.entidad.UserEntity;
import com.nisum.userapi.api.security.TokenInfo;
import io.jsonwebtoken.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.function.Function;


import static com.nisum.userapi.api.utils.Constants.*;


public class TokenProvider {

	private TokenProvider() {
	}

	public static String generateToken(Authentication authentication) {
		// Genera el token con roles, issuer, fecha, expiración (8h)
		final String authorities = authentication.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.joining(","));
		return Jwts.builder()
				.setSubject(authentication.getName())
				.claim(AUTHORITIES_KEY, authorities)
				.signWith(SignatureAlgorithm.HS256, SIGNING_KEY)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setIssuer(ISSUER_TOKEN)
				.setExpiration(new Timestamp(System.currentTimeMillis() + ACCESS_TOKEN_VALIDITY_SECONDS*1000))
				.compact();
	}


	public static TokenInfo generateToken(CustomUserDetails customUserDetails) {
		// Genera el token con roles, issuer, fecha, expiración (8h)
		final String authorities = customUserDetails.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.joining(","));

		Timestamp expirationdate=new Timestamp(System.currentTimeMillis() + ACCESS_TOKEN_VALIDITY_SECONDS*1000);
		return new TokenInfo(Jwts.builder()
				.setSubject(customUserDetails.getUsername())
				.claim(AUTHORITIES_KEY, authorities)
				.signWith(SignatureAlgorithm.HS256,SIGNING_KEY)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setIssuer(ISSUER_TOKEN)
				.setExpiration(expirationdate)
				.compact(),expirationdate);
	}

	public static TokenInfo generateToken(String username) {
		// Genera el token con roles, issuer, fecha, expiración (8h)

		Timestamp expirationdate=new Timestamp(System.currentTimeMillis() + ACCESS_TOKEN_VALIDITY_SECONDS*1000);
		return new TokenInfo(Jwts.builder()
				.setSubject(username)
				.claim(AUTHORITIES_KEY, null)
				.signWith(SignatureAlgorithm.HS256,SIGNING_KEY)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setIssuer(ISSUER_TOKEN)
				.setExpiration(expirationdate)
				.compact(),expirationdate);
	}

	public static TokenInfo generateTokenInfo(Authentication authentication) {
		// Genera el token con roles, issuer, fecha, expiración (8h)
		final String authorities = authentication.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.joining(","));

		Timestamp expirationdate=new Timestamp(System.currentTimeMillis() + ACCESS_TOKEN_VALIDITY_SECONDS*1000);
		return new TokenInfo(Jwts.builder()
				.setSubject(authentication.getName())
				.claim(AUTHORITIES_KEY, authorities)
				.signWith(SignatureAlgorithm.HS256,SIGNING_KEY)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setIssuer(ISSUER_TOKEN)
				.setExpiration(expirationdate)
				.compact(),expirationdate);
	}

	public static UsernamePasswordAuthenticationToken getAuthentication(final String token,
                                                                        final UserDetails userDetails) {

		final JwtParser jwtParser = Jwts.parser().setSigningKey(SIGNING_KEY);

		final Jws<Claims> claimsJws = jwtParser.parseClaimsJws(token);

		final Claims claims = claimsJws.getBody();

		final Collection<SimpleGrantedAuthority> authorities =
				Arrays.stream(claims.get(AUTHORITIES_KEY).toString().split(","))
						.map(SimpleGrantedAuthority::new)
						.collect(Collectors.toList());

		return new UsernamePasswordAuthenticationToken(userDetails, "", authorities);
	}

	public static String getUserName(final String token) {
		final JwtParser jwtParser = Jwts.parser().setSigningKey(SIGNING_KEY);

		final Jws<Claims> claimsJws = jwtParser.parseClaimsJws(token);

		return claimsJws.getBody().getSubject();
	}

	private static Date extractExpiration(String token) {
		return extractClaim(token, Claims::getExpiration);
	}

	private static  <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = extractAllClaims(token);
		return claimsResolver.apply(claims);
	}
	private static Claims extractAllClaims(String token) {
		return Jwts.parser().setSigningKey(SIGNING_KEY).parseClaimsJws(token).getBody();
	}

	private static Boolean isTokenExpired(String token) {
		return extractExpiration(token).before(new Date());
	}

	public static Boolean validateToken(String token, UserDetails userDetails) {
		final String username = getUserName(token);
		return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
	}

}
