package com.nisum.userapi.api.controller;

import com.nisum.userapi.api.bl.service.UserService;
import com.nisum.userapi.api.dao.entidad.UserEntity;
import com.nisum.userapi.api.dto.ResponseList;
import com.nisum.userapi.api.dto.UserDTO;
import com.nisum.userapi.api.dto.UserResponse;
import com.nisum.userapi.api.exception.ValidationException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController
{
    @Autowired
    UserService userService;

    @ApiOperation(value = "Get all users", produces = "application/json")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "user listed successfully.",
                    response = ResponseList.class),
            @io.swagger.annotations.ApiResponse(code = 500, message = "something wrong.",
                    response = String.class),
    })
    @GetMapping("/")
    public ResponseEntity<List<UserDTO>> getAll()
    {
        return new ResponseEntity<>((userService.findAll().stream().map(x -> new UserDTO(x)).collect(Collectors.toList())), HttpStatus.OK);
    }

    @ApiOperation(value = "Create new User", produces = "application/json")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 201, message = "User save successfully.",
                    response = UserResponse.class),
            @io.swagger.annotations.ApiResponse(code = 500, message = "String error",
                    response = String.class),
    })
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/save")
    public ResponseEntity<UserResponse> createUser(@Valid @RequestBody final UserDTO user) throws ValidationException {
        return new ResponseEntity<>(userService.save(user), HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/disable/{id}")
    public ResponseEntity<UserResponse> unActiveUser(@PathVariable final UUID id) throws Exception {
        return new ResponseEntity<>(userService.disable(id), HttpStatus.OK);
    }

}

