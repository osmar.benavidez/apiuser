package com.nisum.userapi.api.controller;

import com.nisum.userapi.api.dto.JwtCredentialsRQ;
import com.nisum.userapi.api.bl.service.JwtAuthService;
import com.nisum.userapi.api.exception.BadCredentialException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AuthenticationController {


    JwtAuthService jwtAuthService;
    final AuthenticationManager authenticationManager;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager,JwtAuthService jwtAuthService) {
        this.authenticationManager = authenticationManager;
        this.jwtAuthService=jwtAuthService;
    }

    @PostMapping(value = "/authenticate")
    public ResponseEntity<?> createAuthentication(@Valid @RequestBody final JwtCredentialsRQ authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        return jwtAuthService.authenticate(authenticationRequest);
    }

    private void authenticate(final String username, final String password) throws BadCredentialException {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new BadCredentialException("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new BadCredentialException("INVALID_CREDENTIALS", e);
        }
    }

}
