package com.nisum.userapi.api.mapper;

import com.nisum.userapi.api.bl.service.CustomUserDetails;
import com.nisum.userapi.api.dao.entidad.UserEntity;
import com.nisum.userapi.api.dao.repository.UserRoleRepository;
import com.nisum.userapi.api.security.CustomUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserDetailsMapper
{
	public static CustomUserDetails builds(UserEntity user, List<UserRoleRepository.UserRoleDTO>userRoleList) {
		return new CustomUser(user.getId(),user.getName(),user.getEmail(),user.getFullname(), user.getPassword(), getAuthorities(userRoleList));
	}

	private static List<GrantedAuthority> getAuthorities(List<UserRoleRepository.UserRoleDTO> userRoleList) {
		List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();

		List<String> listRoles = new ArrayList<>();
		userRoleList.forEach(role->listRoles.add("ROLE_" +role.getRole_Name()));// get role from database - usa il tuo modo **
		grantedAuthorityList = listRoles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
		return grantedAuthorityList;
	}
}
