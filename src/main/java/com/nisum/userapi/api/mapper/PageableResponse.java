package com.nisum.userapi.api.mapper;

import lombok.Data;

import java.util.List;

@Data
public class PageableResponse<T> {
    private List<T> objL;
    private Integer CurrentPage;
    private long TotalItems;
    private Integer TotalPages;

    public PageableResponse(){}

    public PageableResponse(List<T> objL, Integer currentPage, long totalItems, Integer totalPages) {
        this.objL = objL;
        CurrentPage = currentPage;
        TotalItems = totalItems;
        TotalPages = totalPages;
    }
}