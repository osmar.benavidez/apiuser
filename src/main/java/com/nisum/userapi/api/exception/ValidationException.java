package com.nisum.userapi.api.exception;

public class ValidationException extends  Exception {

    public ValidationException(String error){
        super(error);
    }
    public ValidationException(String error, Throwable ex){
        super(error,ex);
    }
    public ValidationException(Throwable cause) {
        super(cause);
    }
}
