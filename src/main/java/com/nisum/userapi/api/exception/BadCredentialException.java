package com.nisum.userapi.api.exception;

public class BadCredentialException extends  Exception {

    public BadCredentialException(String error) {
        super(error);
    }

    public BadCredentialException(String error, Throwable ex) {
        super(error, ex);
    }

    public BadCredentialException(Throwable cause) {
        super(cause);
    }
}