package com.nisum.userapi.api.exception;

import com.nisum.userapi.api.utils.Constants;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * @author Hilario Antonio Campos Silva, 06/04/2022
 * @version 1.0
 * @see AuthenticationEP
 * @see HttpServletRequest
 * @see AuthenticationException
 * @see HttpServletResponse
 * @since 1.0, <i>Esta clase extenderá la clase AuthenticationEntryPoint de Spring y reemplazará su método begin.
 * Rechaza cada solicitud no autenticada y envía el código de error 401.</i>
 */
@Component
public class AuthenticationEP implements AuthenticationEntryPoint, Serializable {


    /**
     * Sobreescritura de metodo.
     * @param request solicitud.
     * @param response response.
     * @param authException exception.
     * @throws IOException checked exception.
     */
    @Override
    public void commence(final HttpServletRequest request, final HttpServletResponse response,
                         final AuthenticationException authException) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.getWriter().write("{ \"mensaje\""+":\""+ Constants.findCauseUsingPlainJava(authException).getMessage() +"\""+"}");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().flush();
    }
}
