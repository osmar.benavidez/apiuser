package com.nisum.userapi.api.exception;

import com.nisum.userapi.api.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

     @ExceptionHandler(Exception.class)
     public final ResponseEntity handleAllExceptions(Exception ex) {
        return  buildResponseEntity(INTERNAL_SERVER_ERROR, Constants.findCauseUsingPlainJava(ex).getMessage());
     }

    @ExceptionHandler(DisabledException.class)
    public ResponseEntity<Object> handleUserDisabledException(DisabledException ex) {
        return buildResponseEntity(INTERNAL_SERVER_ERROR, Constants.findCauseUsingPlainJava(ex).getMessage());
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<Object> handleUsernameNotFoundException(
            UsernameNotFoundException ex) {
        return buildResponseEntity(NOT_FOUND, ex.getMessage());
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
        public final ResponseEntity handleHttpRequestMethodNotSupportedException(Exception ex) {
            return  buildResponseEntity(METHOD_NOT_ALLOWED,Constants.findCauseUsingPlainJava(ex).getMessage());
        }

        @ExceptionHandler(MissingServletRequestParameterException.class)
        protected ResponseEntity<Object> handleMissingServletRequestParameter(
                MissingServletRequestParameterException ex ) {
            String error = ex.getParameterName() + " parameter is missing";
            return buildResponseEntity(BAD_REQUEST, error+":"+Constants.findCauseUsingPlainJava(ex).getMessage());
    }



        @ExceptionHandler(MethodArgumentNotValidException.class)
        protected ResponseEntity<Object> handleMethodArgumentNotValid(
                MethodArgumentNotValidException ex) {
            return buildResponseEntity(BAD_REQUEST,"Validation error:"+Constants.findCauseUsingPlainJava(ex).getMessage());
        }

        /**
         * Handles javax.validation.ConstraintViolationException. Thrown when @Validated fails.
         *
         * @param ex the ConstraintViolationException
         * @return the ApiError object
         */
        @ExceptionHandler(javax.validation.ConstraintViolationException.class)
        protected ResponseEntity<Object> handleConstraintViolation(
                javax.validation.ConstraintViolationException ex) {
            return buildResponseEntity(BAD_REQUEST,"Validation error:"+ Constants.findCauseUsingPlainJava(ex).getMessage());
        }



        /**
         * Handle HttpMessageNotReadableException. Happens when request JSON is malformed.
         *
         * @param ex      HttpMessageNotReadableException
         * @return the ApiError object
         */
        @ExceptionHandler(HttpMessageNotReadableException.class)
        protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex ) {
            String error = "Malformed JSON request";
            return buildResponseEntity(HttpStatus.BAD_REQUEST, error+":"+Constants.findCauseUsingPlainJava(ex).getMessage());
        }

        /**
         * Handle HttpMessageNotWritableException.
         *
         * @param ex      HttpMessageNotWritableException
         * @return the ApiError object
         */
        @ExceptionHandler(HttpMessageNotWritableException.class)
        protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex ) {
            String error = "Error writing JSON output";
            return buildResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR,error+":"+ Constants.findCauseUsingPlainJava(ex).getMessage());
        }

        /**
         * Handle NoHandlerFoundException.
         *
         * @param ex
         * @return
         */
        @ExceptionHandler(NoHandlerFoundException.class)
        protected ResponseEntity<Object> handleNoHandlerFoundException(
                NoHandlerFoundException ex) {
            return buildResponseEntity(NOT_FOUND,String.format("Could not find the %s method for URL %s", ex.getHttpMethod(), ex.getRequestURL()));
        }

        /**
         * Handle javax.persistence.EntityNotFoundException
         */
        @ExceptionHandler(javax.persistence.EntityNotFoundException.class)
        protected ResponseEntity<Object> handleEntityNotFound(javax.persistence.EntityNotFoundException ex) {
            return buildResponseEntity(HttpStatus.NOT_FOUND,Constants.findCauseUsingPlainJava(ex).getMessage());
        }

    /**
     * Handle  ValidationException
     */
        @ExceptionHandler(ValidationException.class)
        protected ResponseEntity<Object> handleValidationException(ValidationException ex) {
            return buildResponseEntity(INTERNAL_SERVER_ERROR,Constants.findCauseUsingPlainJava(ex).getMessage());
        }

    /**
     * Handle  ValidationException
     */
    @ExceptionHandler(BadCredentialException.class)
    protected ResponseEntity<Object> handleBadCredentialException(BadCredentialException ex) {
        return buildResponseEntity(UNAUTHORIZED,Constants.findCauseUsingPlainJava(ex).getMessage());
    }

        /**
         * Handle DataIntegrityViolationException, inspects the cause for different DB causes.
         *
         * @param ex the DataIntegrityViolationException
         * @return the ApiError object
         */
        @ExceptionHandler(DataIntegrityViolationException.class)
        protected ResponseEntity<Object> handleDataIntegrityViolation(DataIntegrityViolationException ex ) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                return buildResponseEntity(HttpStatus.CONFLICT, Constants.findCauseUsingPlainJava(ex).getMessage());
            }
            return buildResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR,Constants.findCauseUsingPlainJava(ex).getMessage());
        }

        /**
         * Handle Exception, handle generic Exception.class
         *
         * @param ex the Exception
         * @return the ApiError object
         */
        @ExceptionHandler(MethodArgumentTypeMismatchException.class)
        protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex ) {
            return buildResponseEntity(BAD_REQUEST,String.format("The parameter '%s' of value '%s' could not be converted to type '%s'", ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName()));
        }

        private ResponseEntity<Object> buildResponseEntity(HttpStatus status,String error)
        {
            ResponseEntity rs= new ResponseEntity<>("{ \"mensaje\""+":\""+error+"\""+"}",status);
            log.error(rs.toString());
            return rs;
        }
}
