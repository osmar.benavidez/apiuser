package com.nisum.userapi.api.bl.service;

import com.nisum.userapi.api.dao.entidad.UserEntity;
import com.nisum.userapi.api.dto.UserDTO;
import com.nisum.userapi.api.dto.UserResponse;
import com.nisum.userapi.api.exception.ValidationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;
import java.util.UUID;

public interface UserService {

    UserResponse save(UserDTO user) throws ValidationException;

    UserResponse disable(UUID userId) throws UsernameNotFoundException, Exception;

    List<UserEntity> findAll();
}
