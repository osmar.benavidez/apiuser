package com.nisum.userapi.api.bl.serviceimpl;

import com.nisum.userapi.api.dto.JwtCredentialsRQ;
import com.nisum.userapi.api.bl.service.CustomUserDetails;
import com.nisum.userapi.api.bl.service.JwtAuthService;
import com.nisum.userapi.api.dao.entidad.SessionEntity;
import com.nisum.userapi.api.dao.entidad.UserEntity;
import com.nisum.userapi.api.dao.repository.SessionRepository;
import com.nisum.userapi.api.dao.repository.UserRepository;
import com.nisum.userapi.api.dao.repository.UserRoleRepository;
import com.nisum.userapi.api.mapper.UserDetailsMapper;
import com.nisum.userapi.api.security.TokenInfo;
import com.nisum.userapi.api.utils.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

import static com.nisum.userapi.api.utils.Constants.TOKEN_BEARER_PREFIX;

@Service
public class JwtAuthServiceImpl implements JwtAuthService {

	private PasswordEncoder passwordEncoder;
	private UserRepository userRepository;
	private UserRoleRepository userRoleRepository;
	private SessionRepository sessionRepository;
	@Autowired
	public JwtAuthServiceImpl( UserRepository userRepository, UserRoleRepository userRoleRepository,SessionRepository sessionRepository) {
		this.passwordEncoder=new BCryptPasswordEncoder();
		this.userRepository = userRepository;
		this.userRoleRepository = userRoleRepository;
		this.sessionRepository=sessionRepository;
	}

	@Override
	public CustomUserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		final UserEntity retrievedUser = userRepository.findByName(userName);
		if (retrievedUser == null) {
			throw new UsernameNotFoundException("Invalid username or password");
		}

		if (!retrievedUser.getIsactive())
		{
			throw new DisabledException("User "
					.concat(retrievedUser.getName())
					.concat(" currently is unactive"));
		}

		List<UserRoleRepository.UserRoleDTO> userRoleDTOList=userRoleRepository.getUserRolesByUserId(retrievedUser.getId());
		//retrievedUser.setPassword(passwordEncoder.encode(retrievedUser.getPassword()));
		return UserDetailsMapper.builds(retrievedUser,userRoleDTOList);
	}

	@Override
	public ResponseEntity<String> authenticate(JwtCredentialsRQ jwtCredentialsRQ)
	{
		CustomUserDetails customUserDetails=loadUserByUsername(jwtCredentialsRQ.getUsername());

		SessionEntity session=sessionRepository.getCurrent(jwtCredentialsRQ.getAppname(),customUserDetails.getUserID());

		TokenInfo tokeninfo=null;
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String jsonToken =null;
		if(session==null)
		{
			tokeninfo=TokenProvider.generateToken(customUserDetails);
			session=SessionEntity.builder()
					.appName(jwtCredentialsRQ.getAppname())
					.userId(customUserDetails.getUserID())
					.token(tokeninfo.getTocken())
					.expirated(tokeninfo.getExpirationDate())
					.isactive(true)
					.build();
			SecurityContextHolder.getContext().setAuthentication(TokenProvider.getAuthentication(tokeninfo.getTocken(),customUserDetails));
			sessionRepository.save(session);
		}else
			tokeninfo=new TokenInfo(session.getToken(),session.getExpirated());

		jsonToken =
				"{\n" +
						"\n\"id\": \""+customUserDetails.getUserID()+"\"," +
						"\n\"fullname\": \""+customUserDetails.getFullName()+"\"," +
						"\n\"username\": \""+customUserDetails.getUsername()+"\"," +
						"\n\"email\": \""+customUserDetails.getEmail()+"\"," +
						"\n\"token\":\""+(TOKEN_BEARER_PREFIX + " " + tokeninfo.getTocken())+"\"," +
						"\n\"expirationdate\":\""+(format.format(tokeninfo.getExpirationDate()))+"\"" +
						"\n" +
						"}";

		return ResponseEntity.ok(jsonToken);
	}

	@Override
	public void flushSession(UUID userid, String token) {
		sessionRepository.flushSession(userid, token);
	}
}
