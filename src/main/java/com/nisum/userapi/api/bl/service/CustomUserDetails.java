package com.nisum.userapi.api.bl.service;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.UUID;

public interface CustomUserDetails extends UserDetails {

    UUID getUserID();
    String getFullName();
    String getEmail();
}
