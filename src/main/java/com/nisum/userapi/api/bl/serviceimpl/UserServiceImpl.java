package com.nisum.userapi.api.bl.serviceimpl;

import com.nisum.userapi.api.bl.service.UserService;
import com.nisum.userapi.api.dao.entidad.PhoneEntity;
import com.nisum.userapi.api.dao.entidad.SessionEntity;
import com.nisum.userapi.api.dao.entidad.UserEntity;
import com.nisum.userapi.api.dao.repository.PhoneRepository;
import com.nisum.userapi.api.dao.repository.SessionRepository;
import com.nisum.userapi.api.dao.repository.UserRepository;
import com.nisum.userapi.api.dto.PhoneDTO;
import com.nisum.userapi.api.dto.UserDTO;
import com.nisum.userapi.api.dto.UserResponse;
import com.nisum.userapi.api.exception.ValidationException;
import com.nisum.userapi.api.security.TokenInfo;
import com.nisum.userapi.api.utils.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nisum.userapi.api.utils.Constants.APPNAME;
import static com.nisum.userapi.api.utils.Constants.PASSWORD_REGEX;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    PhoneRepository phoneRepository;
    @Autowired
    SessionRepository sessionRepository;

    @Override
    @Transactional
    public UserResponse save(UserDTO user) throws ValidationException {
        validate(user);
        UserEntity   userEntity=UserEntity.builder()
                    .fullname(user.getFullname())
                    .name(user.getName())
                    .email(user.getEmail())
                    .password(user.getPassword())
                    .isactive(true)
                    .build();
        Set<PhoneEntity> phones=new HashSet<>();
        for (PhoneDTO phone:user.getPhones()
        ) {
            PhoneEntity phoneEntity=PhoneEntity.builder()
                    .number(phone.getNumber())
                    .cityCode(phone.getCityCode())
                    .countryCode(phone.getCountryCode())
                    .build();
            phoneEntity.setUser(userEntity);
            phones.add(phoneEntity);
        }
        userEntity.setPhones(phones);
        userRepository.save(userEntity);

        TokenInfo tokeninfo= TokenProvider.generateToken(userEntity.getName());
        SessionEntity session= SessionEntity.builder()
                .appName(APPNAME)
                .userId(userEntity.getId())
                .token(tokeninfo.getTocken())
                .expirated(tokeninfo.getExpirationDate())
                .isactive(true)
                .build();
        sessionRepository.save(session);
        return new UserResponse(userEntity.getId(),
                userEntity.getCreationDate(),
                userEntity.getModifiedDate(),
                userEntity.getCreationDate(),
                tokeninfo.getTocken(),
                userEntity.getIsactive());
    }

    @Override
    public UserResponse disable(UUID userId) throws Exception {
        UserEntity userEntity=userRepository.getById(userId);
        if(userEntity==null)
            throw new UsernameNotFoundException("Invalid username or password");
        if(userEntity.getIsactive())
            throw new Exception("User already unactive.");
        userEntity.setIsactive(false);
        userEntity.setModifiedDate((new Timestamp(System.currentTimeMillis())).toLocalDateTime());
        sessionRepository.flushAllUserSession(userId);
        userRepository.save(userEntity);

        return new UserResponse(userEntity.getId(),
                userEntity.getCreationDate(),
                userEntity.getModifiedDate(),
                userEntity.getCreationDate(),
                null,
                userEntity.getIsactive());
    }

    private void validate(UserDTO entity) throws ValidationException {
        Pattern pattern = Pattern.compile(PASSWORD_REGEX);
        Matcher matcher = pattern.matcher(entity.getPassword());
        if (!matcher.matches())
            throw new ValidationException("the password does not meet the established parameters");

        UserEntity user = userRepository.findByEmail(entity.getEmail());
        if (user!=null)
            throw new ValidationException("Error: email already assigned to user"+user.getFullname());
    }

    @Override
    public List<UserEntity> findAll() {
        return userRepository.findAll();
    }
}
