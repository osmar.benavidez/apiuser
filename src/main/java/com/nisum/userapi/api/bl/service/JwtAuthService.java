package com.nisum.userapi.api.bl.service;

import com.nisum.userapi.api.dto.JwtCredentialsRQ;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.UUID;

public interface JwtAuthService  extends UserDetailsService {

    ResponseEntity<String> authenticate(JwtCredentialsRQ jwtCredentialsRQ);

    void  flushSession(UUID userid,  String  token);

}
