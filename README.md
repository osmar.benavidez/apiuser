# Apiuser
 

## Descripcion
Proyecto  para registrar usuarios con sus detalles.
Para registrar usuarios necesitamos autenticarnos con usuario y contrase;a para obtener token que sera usado como parametro en el header de
las peticiones. Este proyecto tiene establecido seguridad JWT
## Pasos para correr proyecto

- [ ] mvn install / mvn clean install  (esto puede ser desde la terminal , 
      previamente tiene que estar configurado maven como variable de entorno)
      Esto hara que el proyecto comienza a descargar todas las dependencias que necesita para poder correr.
- [ ] mvn spring-boot:run
## Recursos expuesto en servidor de aplicaciones
- [ ] [Endpoint](http://localhost:8080/)
- [ ] [H2](http://localhost:8080/h2-console/) 
- [ ] [Swagger UI](http://localhost:8080/swagger-ui/index.html)

## Resumen Consumir Endpoint
- [ ]  Endpoint de Autenticacion para obtener el token
- 
     
      URL:  http://localhost:8080/authenticate
      Tipo: POST
      Parametros:  username: **admin**  password: **123** appname:  **POSTMAN**
      Request: 
      {
      "username":"admin",
      "password":"123",
      "appname":"POSTMAN"
      }

Imagen Adjunta:

![alt text](gitlab/images/authenticate-ep.jpeg)

- [ ]  Endpoint Para crear nuevo Usuario
       
   Solo el usuario que tenga rol ADMIN podra consumir este endpoint

  ####URL:  http://localhost:8080/user/save
  ####Tipo: POST

  #### Parametros:  username: **admin**  , password: **123** , appname:  **POSTMAN**
   
- ####Request:
    
  - {
  
           "fullname": "Juan Rodriguez",
           "name": "jrodriguez",
           "email": "juan@rodriguez.org",
           "password": "MiPeque*123",
           "phones": [
           {
                 "number": "1234567", 
                 "citycode": "1",
                 "contrycode": "57"
            }
        ]
        }

Imagen Adjunta:

![alt text](gitlab/images/createuser-ep.jpeg)







 